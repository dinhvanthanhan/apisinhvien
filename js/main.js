const BASE_URL = "https://633ec06083f50e9ba3b76213.mockapi.io";

var fetchDssvService = function () {
  axios({
    url: `${BASE_URL}/sv`,
    method: "GET",
  })
    .then(function (response) {
      renderDanhSachSinhVien(response.data);
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
};

fetchDssvService();


var renderDanhSachSinhVien = function (listSv) {
  var contentHTML = "";
  listSv.forEach(function (sv) {
    contentHTML += `<tr>
                <td>${sv.ma}</td>
                <td>${sv.ten}</td>
                <td>${sv.email}</td>
                <td>0</td>
                <td>
                <button class="btn btn-primary">Sửa</button>
                <button onclick="xoaSv(${sv.ma})" class="btn btn-danger">Xoá</button>
                </td>
              </tr>`;
  });

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};


var xoaSv = function (idSv) {
 
  axios({
    url: `${BASE_URL}/sv/${idSv}`,
    method: "DELETE",
  })
    .then(function (res) {
      fetchDssvService();
    })
    .catch(function (err) {
        console.log(err);
    });
};


var themSv = function () {
  var sv = layThongTinTuForm();
  console.log("sv: ", sv);

  axios({
    url: `${BASE_URL}/sv`,
    method: "POST",
    data: sv,
  })
    .then(function (res) {
      fetchDssvService();
    })
    .catch(function (err) {
        console.log(err);
    });
};